package com.example.arturolaboratorio3mascotas

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second_view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {
            // validacion de nombre, edad y radioButtons
            var nombre = edtNombre.text.toString()
            var edad = edtEdad.text.toString()

            if (nombre.isEmpty()) {
                Toast.makeText(this, "Ingrese nombre de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()) {
                Toast.makeText(this, "Ingrese edad de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!rbtnPerro.isChecked && !rbtnGato.isChecked && !rbtnConejo.isChecked) {
                Toast.makeText(this, "Seleccione la raza de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //Save Info radioButtons
            var raza = ""
            var imagenRaza : Int = 0
            if (rbtnPerro.isChecked) {
                raza = "Perro"
                imagenRaza = R.drawable.dog_50

            }
            if (rbtnGato.isChecked) {
                raza = "Gato"
                imagenRaza = R.drawable.cat_50
            }
            if (rbtnConejo.isChecked) {
                raza = "Conejo"
                imagenRaza = R.drawable.rabbit_50
            }

            //Save info CheckBoxs

            var vacuna1 = ""
            if (cbxVacuna1.isChecked) {
                vacuna1 = "Tiene Vacuna1"
            }else {
                vacuna1 = "No tiene Vacuna1"
            }

            var vacuna2 = ""
            if (cbxVacuna2.isChecked) {
                vacuna2 = "Tiene Vacuna2"
            }else {
                vacuna2 = "No tiene vacuna2"
            }

            var vacuna3 = ""
            if (cbxVacuna3.isChecked) {
                vacuna3 = "Tiene Vacuna3"
            }else {
                vacuna3 = "No tiene Vacuna 3"
            }

            var vacuna4 = ""
            if (cbxVacuna4.isChecked) {
                vacuna4 = "Tiene Vacuna4"
            }else {
                vacuna4 = "No tiene Vacuna4"
            }

            var vacuna5 = ""
            if (cbxVacuna5.isChecked) {
                vacuna5 = "Tiene Vacuna5"
            }else{
                vacuna5 = "No tiene Vacuna5"
            }

            //Guardando informacion para enviar(Bundle)
            val bundle = Bundle()
            bundle.putString("key_nombre", nombre.toString())
            bundle.putString("key_edad", edad.toString())
            bundle.putString("key_raza", raza.toString())
            bundle.putInt("key_imagenRaza", imagenRaza.toInt())
            bundle.putString("key_Vacuna1", vacuna1)
            bundle.putString("key_Vacuna2", vacuna2)
            bundle.putString("key_Vacuna3", vacuna3)
            bundle.putString("key_Vacuna4", vacuna4)
            bundle.putString("key_Vacuna5", vacuna5)


            /* //Condicional del chechbox
            if (cbxVacuna1.isChecked){
                bundle.putString("key_Vacuna1", cbxVacuna1.isChecked.toString())
            }
            if (cbxVacuna2.isChecked){
                bundle.putString("key_Vacuna2", cbxVacuna2.isChecked.toString())
            }
            if (cbxVacuna3.isChecked){
                bundle.putString("key_Vacuna3", cbxVacuna3.isChecked.toString())
            }
            if (cbxVacuna4.isChecked){
                bundle.putString("key_Vacuna4", cbxVacuna4.isChecked.toString())
            }
            if (cbxVacuna5.isChecked){
                bundle.putString("key_Vacuna1", cbxVacuna5.isChecked.toString())
            }*/


            //Intent para enviar a otra Activity
            val intent = Intent(this, SecondViewActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

    }
}
