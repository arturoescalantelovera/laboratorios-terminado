package com.example.arturolaboratorio3mascotas

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second_view.*

class SecondViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_view)

        val bundleRecepcion = intent.extras

        var nombre = bundleRecepcion!!.getString("key_nombre") ?: "No ingreso el nombre"
        var edad = bundleRecepcion!!.getString("key_edad") ?: "No ingreso la edad"
        var raza = bundleRecepcion!!.getString("key_raza") ?: "No selecciono la raza"
        var imageAnimal = bundleRecepcion!!.getInt("key_imagenRaza") ?: ""
        var vac1 = bundleRecepcion!!.getString("key_Vacuna1")?: ""
        var vac2 = bundleRecepcion!!.getString("key_Vacuna2")?: ""
        var vac3 = bundleRecepcion!!.getString("key_Vacuna3")?: ""
        var vac4 = bundleRecepcion!!.getString("key_Vacuna4")?: ""
        var vac5 = bundleRecepcion!!.getString("key_Vacuna5")?: ""

        tvNombre.text = nombre.toString()
        tvEdad.text = edad.toString()
        tvRaza.text = raza.toString()
        tvVacuna1.text = vac1.toString()
        tvVacuna2.text = vac2.toString()
        tvVacuna3.text = vac3.toString()
        tvVacuna4.text = vac4.toString()
        tvVacuna5.text = vac5.toString()

        var foto = imageAnimal
        imgImagenShow.setImageResource(foto.toString().toInt())


    }
}