package com.example.arturolaboratorio2generaciones

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {
            val anio = edtAnio.text.toString().toInt()

            when (anio) {
                in 1930..1948 -> {
                    tvResultado.text = "Silent Generation con Poblacion: 6'300'000"
                    var imgGeneracion = R.drawable.silent_generation
                    ivImage.setImageResource(imgGeneracion)
                }
                in 1949..1968 -> {
                    tvResultado.text = "Baby Boom con Poblacion: 12'200'000"
                    var imgGeneracion = R.drawable.babyboomers
                    ivImage.setImageResource(imgGeneracion)
                }
                in 1969..1980 -> {
                    tvResultado.text = "Generacion X con Poblacion: 9'300'000"
                    var imgGeneracion = R.drawable.generacion_x
                    ivImage.setImageResource(imgGeneracion)
                }
                in 1981..1993 -> {
                    tvResultado.text = "Generacion Y con Poblacion: 7'200'000"
                    var imgGeneracion = R.drawable.generacion_y
                    ivImage.setImageResource(imgGeneracion)
                }
                in 1994..2010 -> {
                    tvResultado.text = "Generacion Z con Poblacion: 7'800'000"
                    var imgGeneracion = R.drawable.generacion_z
                    ivImage.setImageResource(imgGeneracion)
                }
                else -> {
                    tvResultado.text = "El año ingresado no pertence a ninguna generacion"
                }
            }
        }
    }
}