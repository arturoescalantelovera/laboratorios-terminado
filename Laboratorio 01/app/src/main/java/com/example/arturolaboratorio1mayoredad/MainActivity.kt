package com.example.arturolaboratorio1mayoredad

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesaar.setOnClickListener {
            if (edtEdad.text.toString().toInt() >= 18 ){
                tvResultado.text = "Eres mayor de edad"
            }else{
                tvResultado.text = "Eres menor de edad"
            }

        }
    }
}